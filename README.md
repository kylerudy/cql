CQL Binary Protocol Implementation
==================================

This Haskell library implements Cassandra's CQL Binary Protocol versions
[2] and [3]. It provides encoding and decoding functionality as well
as representations of the various protocol related types.

Contributing
------------

If you want to contribute to this project please read the file
CONTRIBUTING first.

[2]: https://github.com/apache/cassandra/blob/trunk/doc/native_protocol_v2.spec
[3]: https://github.com/apache/cassandra/blob/trunk/doc/native_protocol_v3.spec
